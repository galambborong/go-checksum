package main

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	f, err := os.Open("./file.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	hasher := sha256.New()

	if _, err := io.Copy(hasher, f); err != nil {
		log.Fatal(err)
	}

	value := hex.EncodeToString(hasher.Sum(nil))
	fmt.Println(value)

	error := os.WriteFile("checksum.txt", []byte(value), 0755)

	if error != nil {
		fmt.Printf("Unable to write file: %v", err)
	}
}
